# **PixelItem** 🛡️

<img align="right" src="icon.png">

Pixel-perfect lava weapon and armor sprites.

- 📦 <http://henrysoftware.itch.io/godot-pixel-item>
- 🌐 <http://rakkarage.github.io/PixelItem>
- 📃 <http://guthub.com/rakkarage/PixelItem>

[![.github/workflows/compress.yml](https://github.com/rakkarage/PixelItem/actions/workflows/compress.yml/badge.svg)](https://github.com/rakkarage/PixelItem/actions/workflows/compress.yml)
[![.github/workflows/deploy.yml](https://github.com/rakkarage/PixelItem/actions/workflows/deploy.yml/badge.svg)](https://github.com/rakkarage/PixelItem/actions/workflows/deploy.yml)
